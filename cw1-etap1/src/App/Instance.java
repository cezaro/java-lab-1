package App;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

/**
 * Klasa przechowująca dane wejściowe problemu plecakowego.
 *
 * @author  Cezary Kiljański
 * @version 1.0
 * @since   2019-03-06
 */

public class Instance implements Serializable {
    /** Lista przedmiotów */
    private ArrayList<Item> itemList;

    /** Rozmiar plecaka */
    private int knapsackSize;

    /** Unikalny klucz do serializacji */
    private static final long serialVersionUID = 4385944729110397454L;

    /**
     * Konstruktor.
     * @param itemList Lista przedmiotów
     * @param knapsackSize Rozmiar plecaka
     */
    public Instance(ArrayList<Item> itemList, int knapsackSize) {
        this.itemList = itemList;
        this.knapsackSize = knapsackSize;
    }

    /**
     * Metoda zwracająca listę przedmiotów.
     * @return Zwraca listę przedmiotów
     */
    public ArrayList<Item> getItemList() {
        return this.itemList;
    }

    /**
     * Metoda zwracająca rozmiar plecaka.
     * @return Zwraca rozmiar plecaka
     */
    public int getBackpackSize() {
        return this.knapsackSize;
    }

    public static Instance generate(Integer itemsCount, Integer knapsackSize) {
        ArrayList<Item> newItemList = new ArrayList<>();
        Random generator = new Random();

        if(itemsCount == null)
        {
            itemsCount = generator.nextInt(14) + 1;
        }

        if(knapsackSize == null)
        {
            knapsackSize = generator.nextInt(itemsCount * 10);
        }

        for(int i = 0; i < itemsCount; i++)
        {
            newItemList.add(new Item((generator.nextInt(100) + 1), (generator.nextFloat() * 100)));
        }

        return new Instance(newItemList, knapsackSize);
    }
}
