package App;

import java.util.ArrayList;

/**
 * Klasa abstrakcyjna dla algorytmów.
 *
 * @author  Cezary Kiljański
 * @version 1.0
 * @since   2019-03-06
 */

public abstract class Knapsack {
    /** Lista przedmiotów */
    protected ArrayList<Item> itemList;

    /** Rozmiar plecaka */
    protected int knapsackSize;

    /**
     * Konstruktor
     */
    public Knapsack() {
        this.itemList = new ArrayList<Item>();
    }
    /**
     * Konstruktor
     * @param instance Obiekt klasy Instance z danymi wejściowymi
     */
    public Knapsack(Instance instance) {
        this.itemList = instance.getItemList();
        this.knapsackSize = instance.getBackpackSize();
    }

    /**
     * Metoda abstrakcyjna do znajdywania rozwiązania problemu.
     * @return Zwraca najlepsze znalezione rozwiązanie
     */
    public abstract Solution findSolution();

    /**
     * Metoda zwracająca nazwę algorytmu.
     * @return Zwraca nazwę algorytmu
     */
    public abstract String name();

    /**
     * Metoda zwracająca opis algorytmu.
     * @return Zwraca opis algorytmu
     */
    public abstract String description();

    /**
     * Metoda przypisująca dane wejściowe
     * @param instance Obiekt klasy Instance z danymi wejściowymi
     */
    public void setInstance(Instance instance) {
        this.itemList = instance.getItemList();
        this.knapsackSize = instance.getBackpackSize();
    }
}
