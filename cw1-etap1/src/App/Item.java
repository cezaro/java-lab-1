package App;

import java.io.Serializable;
import java.util.Objects;

/**
 * Klasa przechowująca atrybuty przedmiotu.
 *
 * @author  Cezary Kiljański
 * @version 1.0
 * @since   2019-03-06
 */

public class Item implements Comparable<Item>, Serializable {
    /** Waga przedmiotu */
    private int weight;

    /** Waga przedmiotu */
    private float value;

    /** Unikalny klucz do serializacji */
    private static final long serialVersionUID = -7511379123305616736L;

    /**
     * Konstruktor.
     * @param weight Waga przedmiotu
     * @param value Wartość przedmiotu
     */
    public Item(int weight, float value) {
        this.weight = weight;
        this.value = value;
    }

    /**
     * Metoda zwracająca wagę przedmiotu.
     * @return Zwraca wagę przedmiotu
     */
    public int getWeight() {
        return this.weight;
    }

    /**
     * Metoda zwracająca wartość przedmiotu.
     * @return Zwraca wartość przedmiotu
     */
    public float getValue() {
        return this.value;
    }

    /**
     * Metoda zwracająca stosunek wartości do wagi przedmiotu.
     * @return Zwraca stosunek wartości do wagi przedmiotu
     */
    public float getRatio() {
        return this.value / this.weight;
    }

    /**
     * Metoda porównująca stosunki wartości do wagi dwóch przedmiotów.
     * Używana przy sortowaniu przedmiotów.
     * @see Comparable
     * @return Zwraca liczbę zależnie od tego czy wartości są różne czy takie same
     */
    @Override
    public int compareTo(Item item) {
        return Float.compare(this.getRatio(), item.getRatio());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Item item = (Item) o;
        return weight == item.weight && Float.compare(item.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, value);
    }
}
