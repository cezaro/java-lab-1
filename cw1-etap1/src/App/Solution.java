package App;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Klasa przechowująca najlepsze rozwiązanie dyskretnego problemu plecakowego.
 *
 * @author  Cezary Kiljański
 * @version 1.0
 * @since   2019-03-06
 */

public class Solution implements Serializable {
    /** Lista przedmiotów */
    private ArrayList<Item> itemList;

    /** Waga przedmiotów */
    private int itemsWeight = 0;

    /** Wartość przedmiotów */
    private float itemsValue = 0.0f;

    /** Unikalny klucz do serializacji */
    private static final long serialVersionUID = 6177355727635776407L;

    /**
     * Konstruktor
     */
    public Solution() {
        itemList = new ArrayList<Item>();
    }

    /**
     * Metoda dodaje przedmiot do listy przedmiotów.
     * @param item Przedmiot do dodania
     */
    public void addItem(Item item) {
        itemList.add(item);

        itemsValue += item.getValue();
        itemsWeight += item.getWeight();
    }

    /**
     * Metoda zwracająca listę przedmiotów.
     * @return Lista przedmiotów
     */
    public ArrayList<Item> getItemList() {
        return this.itemList;
    }

    /**
     * Metoda zwracająca wagę przedmiotów.
     * @return Zwraca wagę przedmiotów
     */
    public int getItemsWeight() {
        return this.itemsWeight;
    }

    /**
     * Metoda zwracająca wartość przedmiotów.
     * @return Zwraca wartość przedmiotów
     */
    public float getItemsValue() {
        return this.itemsValue;
    }
}
