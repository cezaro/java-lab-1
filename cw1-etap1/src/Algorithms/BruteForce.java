package Algorithms;

import App.Instance;
import App.Item;
import App.Knapsack;
import App.Solution;

/**
 * Algorytm Brute Force dla dyskretnego problemu plecakowego.
 *
 * @author  Cezary Kiljański
 * @version 1.0
 * @since   2019-03-06
 */

public class BruteForce extends Knapsack {
    /**
     * Konstruktor
     */
    public BruteForce() {
        super();
    }

    /**
     * Konstruktor
     * @param instance Obiekt klasy Instance z danymi wejściowymi
     */
    public BruteForce(Instance instance) {
        super(instance);
    }

    /**
     * Metoda znajduje najlepsze rozwiązanie dla problemu plecakowego.
     * @return Zwraca najlepsze znalezione rozwiązanie
     */
    @Override
    public Solution findSolution() {
        if(this.itemList.size() == 0)
        {
            return null;
        }

        Solution solution = new Solution();
        int n = this.itemList.size();

        for (int i = 0; i < 2 << n - 1; i++)
        {
            Solution actualSolution = new Solution();
            for(int j = 0; j < n; j++)
            {
                if((1 << j & i) > 0)
                {
                    actualSolution.addItem(this.itemList.get(j));
                }
            }

            if(actualSolution.getItemsWeight() <= this.knapsackSize && actualSolution.getItemsValue() > solution.getItemsValue())
            {
                solution = actualSolution;
            }
        }

        return solution;
    }

    /**
     * Metoda zwracająca nazwę algorytmu.
     * @return Zwraca nazwę algorytmu
     */
    @Override
    public String name() {
        return "BruteForce";
    }

    /**
     * Metoda zwracająca opis algorytmu.
     * @return Zwraca opis algorytmu
     */
    @Override
    public String description() {
        return "Algorytm BruteForce sprawdza wszystkie możliwe rozwiązania problemu i wybiera najlepsze.";
    }
}