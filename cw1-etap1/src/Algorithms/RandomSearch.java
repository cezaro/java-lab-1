package Algorithms;

import App.Instance;
import App.Item;
import App.Knapsack;
import App.Solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Algorytm random search dla dyskretnego problemu plecakowego.
 *
 * @author  Cezary Kiljański
 * @version 1.0
 * @since   2019-03-20
 */

public class RandomSearch extends Knapsack {
    /**
     * Konstruktor
     */
    public RandomSearch() {
        super();
    }

    /**
     * Konstruktor
     * @param instance Obiekt klasy Instance z danymi wejściowymi
     */
    public RandomSearch(Instance instance) {
        super(instance);
    }

    /**
     * Metoda znajduje najlepsze rozwiązanie dla problemu plecakowego.
     * @return Zwraca najlepsze znalezione rozwiązanie
     */
    @Override
    public Solution findSolution() {
        Solution solution = new Solution();
        Random rnd = new Random();

        int n = this.itemList.size();

        for(int i = 0; i < 100; i++) {
            ArrayList<Item> newItemList = new ArrayList<Item>(this.itemList);

            Solution tempSolution = new Solution();
            while(newItemList.size() > 0)
            {
                int index = rnd.nextInt(newItemList.size());
                Item item = newItemList.get(index);

                if(solution.getItemsWeight() + item.getWeight() <= this.knapsackSize && solution.getItemList().indexOf(item) == -1)
                {
                    solution.addItem(item);
                }

                newItemList.remove(item);
            }

            if(tempSolution.getItemsValue() > solution.getItemsValue())
            {
                solution = tempSolution;
            }
        }


        return solution;
    }

    /**
     * Metoda zwracająca nazwę algorytmu.
     * @return Zwraca nazwę algorytmu
     */
    @Override
    public String name() {
        return "Random Search";
    }

    /**
     * Metoda zwracająca opis algorytmu.
     * @return Zwraca opis algorytmu
     */
    @Override
    public String description() {
        return "Algorytm Random Search wykonuje 100 iteracji przy czym w każdej wybierany jest losowy zestaw przedmiotów i porównywany z aktualnym najlepszym rozwiązaniem.";
    }
}
