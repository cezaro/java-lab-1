package Algorithms;

import App.Instance;
import App.Item;
import App.Knapsack;
import App.Solution;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Algorytm zachłanny dla dyskretnego problemu plecakowego.
 *
 * @author  Cezary Kiljański
 * @version 1.0
 * @since   2019-03-06
 */

public class Greedy extends Knapsack {
    /**
     * Konstruktor
     */
    public Greedy() {
        super();
    }

    /**
     * Konstruktor
     * @param instance Obiekt klasy Instance z danymi wejściowymi
     */
    public Greedy(Instance instance) {
        super(instance);
    }

    /**
     * Metoda znajduje najlepsze rozwiązanie dla problemu plecakowego.
     * @return Zwraca najlepsze znalezione rozwiązanie
     */
    @Override
    public Solution findSolution() {
        Solution solution = new Solution();

        int n = this.itemList.size();
        ArrayList<Item> newItemList = new ArrayList<Item>(this.itemList);

        Collections.sort(newItemList);

        for(int i = n - 1; i >= 0; i--)
        {
            Item item = newItemList.get(i);

            if(solution.getItemsWeight() + item.getWeight() <= this.knapsackSize)
            {
                solution.addItem(item);
            }
        }

        return solution;
    }

    /**
     * Metoda zwracająca nazwę algorytmu.
     * @return Zwraca nazwę algorytmu
     */
    @Override
    public String name() {
        return "Greedy";
    }

    /**
     * Metoda zwracająca opis algorytmu.
     * @return Zwraca opis algorytmu
     */
    @Override
    public String description() {
        return "Algorytm Greedy (zachłanny) w każdym kroku wybiera rozwiązanie najbardziej rokujące.";
    }
}
