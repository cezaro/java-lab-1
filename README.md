# Laboratorium 1 - Własna biblioteka, javadoc, JavaFX oraz internacjonalizacja

## Etap 1
Biblioteka do rozwiązywania dyskretnego problemu.
Zaimplementowane algorytmy:
- Bruteforce
- Greedy
- Random Search

Biblioteka posiada opisany i wygenerowany JavaDoc.

## Etap 2
Aplikacja w JavaFX która korzysta z biblioteki z etapu 1 do rozwiązywania problemu plecakowego. 