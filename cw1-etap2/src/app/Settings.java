package app;

import java.io.*;
import java.util.ArrayList;

public class Settings {
    private static String fileName = "settings.ini";

    public boolean saveSettings(ArrayList<String> settings) {
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for(String line : settings)
            {
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        }
        catch(IOException ex) {
            System.out.println("Wystąpił problem podczas zapisywania pliku");
        }

        return true;
    }

    public ArrayList<String> loadSettings() {
        ArrayList<String> settings = new ArrayList<String>();

        String line = null;
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                settings.add(line);
            }

            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Nie znaleziono pliku");
            return null;
        }
        catch(IOException ex) {
            System.out.println("Wystąpił problem podczas odczytywania pliku");
            return null;
        }

        return settings;
    }

    public static boolean deleteFile() {
        File file = new File(fileName);

        if(!file.delete())
        {
            return false;
        }

        return true;
    }
}
