package app;

import Algorithms.BruteForce;
import Algorithms.Greedy;
import Algorithms.RandomSearch;
import App.Instance;
import App.Item;
import App.Knapsack;
import App.Solution;
import javafx.application.Platform;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller implements Initializable {
    @FXML private VBox root;

    @FXML private TableView<Item> itemsTable;

    @FXML private TableColumn<Item, Integer> columnIndex;
    @FXML private TableColumn<Item, Integer> columnWeight;
    @FXML private TableColumn<Item, String> columnValue;
    @FXML private TableColumn<Item, String> columnRatio;

    @FXML private TextField weightInput;
    @FXML private TextField valueInput;
    @FXML private TextField knapsackSizeInput;

    @FXML private Label descriptionLabel;

    @FXML private Button addItemButton;

    @FXML private ComboBox algorithmSelect;

    @FXML private Text dateText;
    @FXML private Text itemsCount;

    private Stage aboutWindow;

    private ArrayList<Item> itemList = new ArrayList<Item>();
    private Solution solution;

    private Locale locale = Locale.getDefault();
    private ResourceBundle messages;

    private Settings settings = new Settings();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        algorithmSelect.getItems().add(0,"BruteForce");
        algorithmSelect.getItems().add(1, "Greedy");
        algorithmSelect.getItems().add(2, "Random Search");
        algorithmSelect.setValue(algorithmSelect.getItems().get(0));

        updateDescription();

        if(messages == null)
        {
            messages = ResourceBundle.getBundle("bundles.MessagesBundle", locale);
        }

        ArrayList<String> settingsStrings = settings.loadSettings();
        if(settingsStrings != null) {
           this.decodeData(settingsStrings);
        }

        columnIndex.setCellValueFactory(cellData -> {
            Item item = cellData.getValue();

            return new SimpleIntegerProperty(itemList.indexOf(item) + 1).asObject();
        });

        columnWeight.setCellValueFactory(new PropertyValueFactory<Item, Integer>("weight"));
        columnValue.setCellValueFactory(cellData -> {
            Item item = cellData.getValue();

            return new SimpleStringProperty(String.format(locale,"%.2f", item.getValue()));
        });

        columnRatio.setCellValueFactory(cellData -> {
            Item item = cellData.getValue();

            return new SimpleStringProperty(String.format(locale,"%.3f",item.getRatio()));

        });

        updateTable();

        insertDate();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                setClasses(false);
            }
        });

        algorithmSelect.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue value, String oldValue, String newValue) {
                updateDescription();
            }
        });
    }

    public boolean addItemButton(ActionEvent actionEvent) {
        if (weightInput.getText().equals("") || valueInput.getText().equals("")) {
            return false;
        }

        Integer weight = Integer.valueOf(weightInput.getText());
        Float value = Float.valueOf(valueInput.getText().replace(",", "."));

        if (!this.addItem(weight, value)) {
            return false;
        }

        valueInput.setText("");
        weightInput.setText("");

        return true;
    }

    public boolean findSolutionEvent(ActionEvent actionEvent) {
        String knapsackSize = knapsackSizeInput.getText();

        if (knapsackSize.equals("")) {
            return false;
        }

        this.setClasses(true);

        Instance instance = new Instance(itemList, Integer.valueOf(knapsackSize));
        Knapsack algorithm = this.getAlgorithm();

        algorithm.setInstance(instance);

        solution = algorithm.findSolution();

        addItemButton.setDisable(true);
        this.setClasses(false);

        return true;
    }

    public void openInfoWindow(ActionEvent actionEvent) throws IOException {
        if (aboutWindow == null) {
            Parent root = FXMLLoader.load(getClass().getResource("../views/about.fxml"), messages);

            aboutWindow = new Stage();
            aboutWindow.setScene(new Scene(root));
            aboutWindow.setTitle(messages.getString("menuAbout"));
        }

        aboutWindow.show();
    }

    public void closeAplication(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void clearForm(ActionEvent actionEvent) {
        itemList.clear();
        updateTable();

        addItemButton.setDisable(false);
        this.setClasses(true);
    }

    public void changeLanguageToPL(ActionEvent actionEvent) throws IOException {
        locale = Locale.getDefault();

        loadLanguage(actionEvent);
    }

    public void changeLanguageToGB(ActionEvent actionEvent) throws IOException {
        locale = new Locale("en", "GB");

        loadLanguage(actionEvent);
    }

    public void changeLanguageToUS(ActionEvent actionEvent) throws IOException {
        locale = new Locale("en", "US");

        loadLanguage(actionEvent);
    }

    public void insertTestData(ActionEvent actionEvent) {
        knapsackSizeInput.setText("15");
        itemList = new ArrayList<Item>() {
            {
                add(new Item(1, 2));
                add(new Item(12, 4));
                add(new Item(4, 10));
                add(new Item(1, 1));
                add(new Item(2, 2));

            }
        };

        updateTable();
    }

    private void setClasses(boolean clearClasses) {
        int i = 0;

        for (Node n : itemsTable.lookupAll("TableRow")) {
            if (n instanceof TableRow && i < itemList.size()) {
                TableRow row = (TableRow) n;

                Item item = itemsTable.getItems().get(i);

                if (clearClasses) {
                    row.getStyleClass().clear();
                } else if (isInSolution(item))
                    row.getStyleClass().add("selected");

                i++;
            }
        }
    }

    private boolean addItem(Integer weight, Float value) {
        Item item = new Item(weight, value);

        if (itemList.indexOf(item) != -1) {
            return false;
        }

        itemList.add(item);
        updateTable();

        return true;
    }

    private boolean isInSolution(Item item) {
        return solution != null && solution.getItemList().indexOf(item) != -1 ? true : false;
    }

    private Knapsack getAlgorithm() {
        Knapsack algorithm = null;
        String algorithmName = algorithmSelect.getValue().toString();

        if (algorithmName.indexOf("BruteForce") != -1) {
            algorithm = new BruteForce();
        } else if (algorithmName.indexOf("Greedy") != -1) {
            algorithm = new Greedy();
        } else {
            algorithm = new RandomSearch();
        }

        return algorithm;
    }

    private void insertDate() {
        LocalDate date = LocalDate.now();

        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, locale);

        this.dateText.setText(formatter.format(new Date()));
    }

    private void loadLanguage(ActionEvent actionEvent) throws IOException {
        settings.saveSettings(encodeData());

        Scene scene = root.getScene();

        messages = ResourceBundle.getBundle("bundles.MessagesBundle", locale);
        Parent view = FXMLLoader.load(getClass().getResource("../views/view.fxml"), messages);

        scene.setRoot(view);
    }

    private ArrayList<String> encodeData() {
        ArrayList<String> data = new ArrayList<String>();

        int index = this.algorithmSelect.getItems().indexOf(this.algorithmSelect.getValue());

        data.add("language=" + locale.getLanguage() + "_" + locale.getCountry());
        data.add("knapsackSize=" + this.knapsackSizeInput.getText());
        data.add("algorithm=" + index);

        for(Item i : this.itemList) {
            data.add("item=" + i.getWeight() + "|" + i.getValue());
        }

        if(solution != null) {
            for(Item i : this.solution.getItemList())
            {
                data.add("solutionItem=" + i.getWeight() + "|" + i.getValue());
            }
        }

        return data;
    }

    private boolean decodeData(ArrayList<String> settings) {
        solution = new Solution();

        for(String string : settings) {
            Pattern compiledPattern = Pattern.compile("([\\w]+)=(.+)");
            Matcher matcher = compiledPattern.matcher(string);

            if(matcher.matches()) {
                switch (matcher.group(1)) {
                    case "language" : {
                        String[] parts = matcher.group(2).split("[_]");
                        locale = new Locale(parts[0], parts[1]);

                        messages = ResourceBundle.getBundle("bundles.MessagesBundle", locale);
                        break;
                    }
                    case "knapsackSize": {
                        knapsackSizeInput.setText(matcher.group(2));
                        break;
                    }
                    case "algorithm": {
                        algorithmSelect.setValue(algorithmSelect.getItems().get(Integer.valueOf(matcher.group(2))));
                        break;
                    }
                    case "item": {
                        String[] parts = matcher.group(2).split("[|]");

                        addItem(Integer.valueOf(parts[0]), Float.valueOf(parts[1]));
                        break;
                    }
                    case "solutionItem": {
                        String[] parts = matcher.group(2).split("[|]");

                        solution.addItem(new Item(Integer.valueOf(parts[0]), Float.valueOf(parts[1])));
                        break;
                    }
                }
            }
        }

        if(solution.getItemList().size() == 0) {
            solution = null;
        }

        return true;
    }

    private void updateTable() {
        ObservableList<Item> observableList = FXCollections.observableList(itemList);
        itemsTable.setItems(observableList);

        updateItemsCount();
    }

    private void updateItemsCount() {
        String itemsString = messages.getString("itemsCount"),
                correctString = null;

        String[] parts = itemsString.split("[|]");

        if(locale.getCountry().equals("PL")) {
            if(itemList.size() == 1)
                correctString = parts[0];
            else if(itemList.size() < 5 && itemList.size() > 1)
                correctString = parts[1];
            else
                correctString = parts[2];
        } else {
            if(itemList.size() == 1)
                correctString = parts[0];
            else
                correctString = parts[1];
        }

        itemsCount.setText(itemList.size() + " " + correctString);
    }

    private void updateDescription() {
        descriptionLabel.setText(this.getAlgorithm().description());
    }
}
