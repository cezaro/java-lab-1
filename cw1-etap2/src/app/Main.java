package app;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Locale locale = Locale.getDefault();

        ResourceBundle resources = ResourceBundle.getBundle("bundles.MessagesBundle", locale);

        Parent root = FXMLLoader.load(getClass().getResource("../views/view.fxml"), resources);

        primaryStage.setTitle("Cezary Kiljański - Ćwiczenie 1");

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/css/style.css");
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.getIcons().add(new Image("/img/backpack.png"));
    }


    public static void main(String[] args) {
        Settings.deleteFile();

        launch(args);
    }
}
